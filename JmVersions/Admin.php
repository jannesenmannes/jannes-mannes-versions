<?php
/**
 * Created by PhpStorm.
 * User: Thomas van der Westen
 * Date: 13/02/17
 * Time: 18:04
 */

namespace JmVersions;

class Admin {
	private static $options;
	const option = 'jm_versions';
	const settings_group = 'jm_versions_settings_group';

	public static function init() {
		add_action( 'admin_menu', array( get_called_class(), 'add_menu_page' ) );
		add_action( 'admin_init', array( get_called_class(), 'register_settings' ) );
	}

	public static function add_menu_page() {
		add_menu_page( _x( 'Versions', '', Plugin::text_domain ), 'Versions', 'manage_options', Plugin::text_domain,
			array(
				get_called_class(),
				'auto_publisher_page'
            ) );
	}

	public static function get_options() {
		if ( ! is_null( self::$options ) ) {
			return self::$options;
		}

		return self::$options = get_option( self::option );
	}

	public static function auto_publisher_page() {
		// Set class property
		self::get_options();
		?>
        <div class="wrap">
            <h1><?php _ex( 'Jannes & Mannes Versions', 'page title', Plugin::text_domain ) ?></h1>
            <form method="post" action="options.php">
				<?php
				// This prints out all hidden setting fields
				settings_fields( self::settings_group );
				do_settings_sections( self::settings_group );
				submit_button();
				?>
            </form>
        </div>
		<?php
	}

	public static function register_settings() {
		//register our settings
		register_setting( self::settings_group, self::option, array( get_called_class(), 'sanitize' ) );

		add_settings_section( 'section_token', // ID
			'Token', // Title
			array( get_called_class(), 'print_section_info' ), // Callback
			self::settings_group // Page
		);

		add_settings_field( 'section_token', 'Token', array(
			get_called_class(),
			'jm_versions_token_callback'
		), self::settings_group, 'section_token' );
	}

	/**
	 * Print the Section text
	 */
	public static function print_section_info() {
		_ex( 'Set your API key', 'Admin page', Plugin::text_domain );
	}

	public static function jm_versions_token_callback() {
		if ( ! isset( self::$options['jm_versions_token'] ) ) {
			self::$options['jm_versions_token'] = '';
		}
		printf( '<input type="text" name="%s[jm_versions_token]" id="jm_versions_token" value="%s" />', self::option,
			self::$options['jm_versions_token'] );
	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param array $input Contains all settings fields as array keys
	 *
	 * @return array
	 */
	public static function sanitize( $input ) {

		$new_input = array();

		if ( isset( $input['jm_versions_token'] ) ) {
			$new_input['jm_versions_token'] = $input['jm_versions_token'];
		}

		return $new_input;
	}
}