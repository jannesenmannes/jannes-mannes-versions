<?php
/**
 * Created by PhpStorm.
 * User: janhenkes
 * Date: 11/10/16
 * Time: 10:34
 */

namespace JmVersions;

use WP_REST_Request;
use Defuse\Crypto\Crypto;

class Plugin {
	const text_domain = 'jm_versions';

	public static function init() {
		Admin::init();
		Updates::add_hooks();

		add_action( 'rest_api_init', array( get_called_class(), 'register_api_route' ) );
	}

	public static function register_api_route() {
		register_rest_route( 'jm-versions/v1', '/check', array(
			'methods'  => 'POST',
			'callback' => array( get_called_class(), 'get_post_objects' ),
			'permission_callback' => array( get_called_class(), 'validate_token' ),
		) );
	}

	public static function validate_token( $request ) {
		$headers  = $request->get_headers();
		$settings = get_option( 'jm_versions' );
		$response = false;

		if ( isset( $headers['x_jmversions_token'][0] ) && isset( $settings[ 'jm_versions_token' ] ) ) {
			if ( $settings[ 'jm_versions_token' ] === $headers['x_jmversions_token'][0] ) {
				$response = true;
			}
		}

		return $response;
	}


	public static function get_post_objects( WP_REST_Request $request ) {
		$response['wordpress']['version']     = get_bloginfo( 'version' );
		$response['wordpress']['name']        = get_bloginfo( 'name' );
		$response['wordpress']['admin_email'] = get_bloginfo( 'admin_email' );
		$response['wordpress']['url']         = get_bloginfo( 'url' );
		$response['plugins']                  = self::get_plugins_obj();

		return $response;
	}

	public static function get_plugins_obj() {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}
		$response = array();

		$all_plugins = get_plugins();

		foreach ( $all_plugins as $plugin_slug => $plugin ) {
			$slug              = dirname( $plugin_slug );
			$response[ $slug ] = $plugin['Version'];
		}

		return $response;

	}
}