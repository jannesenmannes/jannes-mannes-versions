<?php
/**
 * Created by PhpStorm.
 * User: janhenkes
 * Date: 22/05/17
 * Time: 16:34
 */

namespace JmVersions;


class Updates {
	public static function add_hooks() {
		// append plugin information
		// Note: is_admin() was used previously, however this prevents jetpack manage & ManageWP from working
		//add_filter( 'plugins_api', array( get_called_class(), 'modify_plugin_details' ), 20, 3 );

		// append update information
		add_filter( 'pre_set_site_transient_update_plugins', array( get_called_class(), 'modify_plugin_update' ) );
		add_filter( 'jannes_mannes_versions/updates/plugin_update', array( get_called_class(), 'plugin_update' ), 10, 2 );
	}

	public static function modify_plugin_details() {
	}

	public static function get_remote_plugin_info() {
		// create basic version of plugin info.
		// this should replicate the data available via plugin_api()
		// doing so allows ACF PRO to load data from external source
		$info = array(
			'version'        => '',
			'changelog'      => '',
			'upgrade_notice' => ''
		);

		// get readme
		$response = wp_safe_remote_get( 'https://bitbucket.org/jannesenmannes/jannes-mannes-versions/raw/master/readme.txt' );

		// bail early if no response
		if ( is_wp_error( $response ) || empty( $response['body'] ) ) {
			return $info;
		}

		// use regex to find upgrade notice
		$matches = null;
		$regexp  = '/(== Changelog ==)([\s\S]+?)(==|$)/';

		// bail early if no match
		if ( ! preg_match( $regexp, $response['body'], $matches ) ) {
			return $info;
		}

		// convert to html
		$text = wp_kses_post( trim( $matches[2] ) );

		// pretify
		$text = preg_replace( '/^= (.*?) =/m', '<h4>$1</h4>', $text );
		$text = preg_replace( '/^[\*] (.*?)(\n|$)/m', '<li>$1</li>', $text );
		$text = preg_replace( '/\n<li>(.*?)/', "\n" . '<ul><li>$1', $text );
		$text = preg_replace( '/(<\/li>)(?!<li>)/', '$1</ul>' . "\n", $text );

		preg_match( "/= (.*?) =/", trim( $matches[2] ), $output_array );
		$info['version'] = $output_array[1];

		// update
		$info['upgrade_notice'] = $text;

		// return
		return $info;
	}

	public static function is_update_available() {
		$info = self::get_remote_plugin_info();
		if ( ! file_exists( __DIR__ . '/../jm-versions.php' ) ) {
			return false;
		}
		$current_info = get_plugin_data( __DIR__ . '/../jm-versions.php', $markup = false, $translate = false );
		$version      = $current_info['Version'];

		// return false if no info
		if ( empty( $info['version'] ) ) {
			return false;
		}

		// return false if the external version is '<=' the current version
		if ( version_compare( $info['version'], $version, '<=' ) ) {
			return false;
		}

		// return
		return true;
	}

	public static function plugin_update( $update, $transient ) {
		// bail early if no update available
		if ( ! self::is_update_available() ) {
			return false;
		}

		// vars
		$info     = self::get_remote_plugin_info();
		$basename = self::get_basename();
		$slug     = 'jannes-mannes-versions';

		// create new object for update
		$obj              = new \stdClass();
		$obj->slug        = $slug;
		$obj->plugin      = $basename;
		$obj->new_version = $info['version'];
		$obj->url         = 'https://jannesmannes.nl';
		$obj->package     = 'https://bitbucket.org/jannesenmannes/jannes-mannes-versions/raw/master/jannes-mannes-versions.zip';

		// return
		return $obj;
	}

	private static function is_plugin_active() {
		// vars
		$basename = self::get_basename();

		// ensure is_plugin_active() exists (not on frontend)
		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		// return
		return is_plugin_active( $basename );
	}

	private static function get_basename() {
		return 'jannes-mannes-versions/jm-versions.php';
	}

	private static function maybe_get( $array, $key = 0, $default = null ) {
		// if exists
		if ( isset( $array[ $key ] ) ) {
			return $array[ $key ];
		}

		// return
		return $default;
	}

	public static function modify_plugin_update( $transient ) {
		// bail early if no response (dashboard showed an error)
		if ( ! isset( $transient->response ) ) {
			return $transient;
		}

		// vars
		$basename     = self::get_basename();
		$show_updates = true;

		// bail early if not a plugin (included in theme)
		if ( ! self::is_plugin_active() ) {
			$show_updates = false;
		}

		// bail early if no show_updates
		if ( ! $show_updates ) {
			// remove from transient
			unset( $transient->response[ $basename ] );

			// return
			return $transient;
		}

		// get update
		$update = self::maybe_get( $transient->response, $basename );

		// filter
		$update = apply_filters( 'jannes_mannes_versions/updates/plugin_update', $update, $transient );

		// update
		if ( $update ) {
			$transient->response[ $basename ] = $update;
		} else {
			unset( $transient->response[ $basename ] );
		}

		// return
		return $transient;
	}
}