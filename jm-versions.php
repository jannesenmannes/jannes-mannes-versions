<?php
/*
Plugin Name: Jannes & Mannes Versions
Plugin URI:
Description:
Author: Jannes & Mannes
Version: 1
Author URI: https://www.jannesmannes.nl
*/

require __DIR__ . '/vendor/autoload.php';

\JmVersions\Plugin::init();