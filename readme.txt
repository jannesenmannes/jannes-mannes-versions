=== Jannes & Mannes Versions ===
Contributors: jannesmannes
Tags:
Requires at least: 3.0.1
Tested up to: 4.7.5
Stable tag: 4.7.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

== Installation ==

== Frequently Asked Questions ==

= How do I make a ZIP of the plugin =
 rm jannes-mannes-versions.zip
 zip -rX jannes-mannes-versions.zip *

== Changelog ==

= 1 =
Fix: replace all bracket arrays with old array() functions for support for old PHP versions.

= 0.6 =

= 0.5 =
ZIP command:
rm jannes-mannes-versions.zip; cd ../; zip -r jannes-mannes-versions.zip jannes-mannes-versions -x *.git* "*\.idea*" *.DS_Store; mv jannes-mannes-versions.zip jannes-mannes-versions; cd jannes-mannes-versions;

= 0.4 =

= 0.3 =
Update script

= 0.2 =
Bugfixes

= 0.1 =
This is the first version of the plugin.